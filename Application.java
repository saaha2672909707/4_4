import model.packaging.Box;
import model.part.Cube;
import model.part.Part;
import model.part.Sphere;
import model.part.Tetrahedron;

import java.util.Collection;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        // Упустимо код отримання списку деталей, задамо його безпосерезньо перед викликом потрібного коду

        // Деталі
        Collection<Part> partList = List.of(   // list1 на partList
                new Cube(450, 0.1),
                new Cube(300, 0.2),
                new Sphere( 150, 0.3),
                new Tetrahedron(250, 0.2),
                new Cube(350, 0.2),
                new Sphere(100, 0.1)
        );

        // Коробки
        Collection<Box> boxList = List.of(     //list2 на boxList
                new Box(700),
                new Box(500),
                new Box(500),
                new Box(500),
                new Box(400),
                new Box(400),
                new Box(400),
                new Box(300),
                new Box(300),
                new Box(200)
        );

        int boxPackagingPricePerMeter = 40; // в копійках

        new PartsProcessor().packageParts(partList, boxList, boxPackagingPricePerMeter);
        // Підказка: в цьому виклиці складно розібратися з сігнатурою через погані імена зммінних. (виконав)
    }

}
